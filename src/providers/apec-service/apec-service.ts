import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApecServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApecServiceProvider {

	//private url = "https://randomuser.me/api/?results=25"
	private url = "http://adamix.net/api/apec"

  constructor(public http: HttpClient) {
  }

  loginUser(endpoint, email, clave){
  	let url = `${this.url}/${endpoint}?email=${email}&clave=${clave}`;
  	return this.http.get(url);
  }

  isLoggedIn(){
    let user = localStorage.getItem("user")
    if (user) {
      return true;
    }
    return false;
  }

  getMyOrders(endpoind, userName){
    let url = `${this.url}/${endpoind}?user=${userName}`;
    return this.http.get(url);
  }

  createNewOrder(endpoint, orderData){
    let user = orderData.user;
    let title = orderData.title;
    let detail = orderData.detail;
    let monto = Number(orderData.monto);
    let url = `${this.url}/${endpoint}?user=${user}&titulo=${title}&detalle=${detail}&monto=${monto}`;
    return this.http.get(url);
  }

  createUser(endpoint, email, name, password){
    let url = `${this.url}/${endpoint}?email=${email}&nombre=${name}&clave=${password}`
    return this.http.get(url)
  }

  resetPassword(endpoint, email){
    let url = `${this.url}/${endpoint}?email=${email}`
    return this.http.get(url)
  }
}
