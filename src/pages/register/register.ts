import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { ApecServiceProvider } from '../../providers/apec-service/apec-service'
import { LoginPage } from '../login/login'


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
	email: string = "";
  password: string = "";
	name: string = "";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public apecService: ApecServiceProvider,
              public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad RegisterPage');
  }

  register(){
    let seq = this.apecService.createUser('createUser', this.email, this.name, this.password);
    seq.subscribe(
      (res:any) => {
        let message = "";
        if(res.ok){
          message = `Usuario creado con éxito`;
          this.navCtrl.push(LoginPage);
          let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
          });
          toast.present();
        } else {
          // Usuario o clave no validas
          let errors = res.error
          alert(errors)
        }
      }, 
      (error) => {
        console.log(error);
        let toast = this.toastCtrl.create({
          message: "Error conectando con el servicio",
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    );
  }
}
