import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ToastController } from 'ionic-angular'
import { ApecServiceProvider } from '../../providers/apec-service/apec-service'

import { ListPage } from "../list/list"
import { RegisterPage } from "../register/register"


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: {id: "", nombre: "", email: ""}
  email: string = "";
  password: string = "";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public apecService: ApecServiceProvider) {
  }

  /*
  ionViewDidLoad() {
    this.apecService.getUsers()
    .subscribe(
      (data) => {
        let users = data["results"];
        console.log(users);
      },
      (error) => {
        console.log(error);
      }
    )
  }
  */

  doLogin(){
    if (this.validateData()){
      let seq = this.apecService.loginUser("login", this.email, this.password)
      seq.subscribe(
        (res:any) => {
          //console.log(res);
          let message = "";
          if(res.ok){
            let userData = res.data;
            this.user = {...userData}
            //console.log(this.user);
            localStorage.setItem('user', JSON.stringify(this.user));
            message = `Bienvenido ${userData.nombre}`
            this.navCtrl.push(ListPage);
          } else {
            // Usuario o clave no validas
            let errors = res.error
            console.log(errors)
            message = "Usuario o clave no válidos"
          }
          let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
          });
          toast.present();
        },
        (error) => {
          console.log(error);
          let toast = this.toastCtrl.create({
            message: "Error conectando con el servicio",
            duration: 3000,
            position: 'top'
          });
          toast.present();
       });
    }
  }

  goRegister(){
    this.navCtrl.push(RegisterPage);
  }

  recoverPassword(){
    if (this.email == "") {
      alert("Email requerido");
    } else {
     let seq = this.apecService.resetPassword('resetPwd', this.email);
     seq.subscribe(
       (res:any) => {
         if (res.ok){
           console.log(res)
           let message = res.mensaje[0];
           alert(message);
         }
       },
       (error:any) => {
         console.log(error);
         alert("Error conectando con el servicio");
       }
     );
    }
  }

  validateData(){
    if (this.email == "" || this.password == ""){
      let toast = this.toastCtrl.create({
        message: "Datos Requeridos",
        duration: 3000,
        position: 'top'
      });
      toast.present();
      return false
    }
    return true;
  }
}
