import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { ApecServiceProvider } from '../../providers/apec-service/apec-service'


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	user: any
	order = {user: "", title: "", detail: "", monto: 0};

  constructor(public navCtrl: NavController,
  						public alertCtrl: AlertController,
  						public apecService: ApecServiceProvider) {
  }

 ionViewDidLoad(){}

 createOrder(){
 	this.user = JSON.parse(localStorage.getItem("user"))  // if no user, user: null
 	this.order.user = this.user.nombre;
 	if (this.validateData()){
	 	let seq = this.apecService.createNewOrder("createOrder", this.order)
	 	seq.subscribe(
	 		(res:any) => {
	 			let message;
	 			if (res.ok) { 				
	 				message = `Orden ${this.order.title} creada con éxito`;
	 				this.order.title = "";
	 				this.order.detail = "";
	 				this.order.monto = 0;
	 			} else {
	 				//console.log(res)
	 				message = "" + res.error;
	 			}
	 			let ionicAlert = this.alertCtrl.create({
					title: "Nueva Orden",
					subTitle: message,
					buttons: ['Ok']
				 });
				ionicAlert.present();
	 		},
	 		(error:any) => {
	 			let message = "Error conectando con el servicio";
	 			console.log(error)
	 			let ionicAlert = this.alertCtrl.create({
					title: "Error",
					subTitle: message,
					buttons: ['Ok']
				 });
				ionicAlert.present();
	 		}
	 	);
 	} else {
	 	let message = "Completar todos los datos";
		let ionicAlert = this.alertCtrl.create({
			title: "Error",
			subTitle: message,
			buttons: ['Ok']
		 });
		ionicAlert.present();
	 	}
 }

 validateData(){
 	if(this.order.user === "" || 
 		 this.order.title === "" ||
 		 this.order.detail === "" ||
 		 this.order.monto === 0){
 		return false;
 	}
 	return true;
 }
}
