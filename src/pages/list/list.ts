import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { ApecServiceProvider } from '../../providers/apec-service/apec-service'


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  user: any;
  noData = true;
  orders: Array<{id: string, titulo: string, estado: string, 
                 detalle: string, monto: string, user: string}>;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public apecService: ApecServiceProvider) {  
  }

  ionViewDidLoad(){
    this.orders = []
    this.user = JSON.parse(localStorage.getItem("user"))  // if no user, user: null
    let seq = this.apecService.getMyOrders("getMyOrders", this.user.nombre)
    seq.subscribe(
      (res:any) => {
        if(res.ok){
          this.noData = false;
          this.orders = res.data;
        } else {
          let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: "Error obteniendo datos del servicio.",
            buttons: ['Ok']
          });
          alert.present();
        }
      },
      (error:any) => {
        this.noData = true;
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: "Error conectando con el servicio.",
          buttons: ['Ok']
        });
        alert.present();
      }
    );
  }

  orderClick(order:any){
    let alertTitle = `${order.titulo}`;
    let alertMessage = `
      <br/>
      Estado: ${order.estado} <br/>
      Detalle: ${order.detalle} <br/>
      Monto: ${order.monto}
    `
    let alert = this.alertCtrl.create({
      title: alertTitle,
      subTitle: alertMessage,
      buttons: ['Ok']
    });
    alert.present();
  }
}
